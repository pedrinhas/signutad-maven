/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.utad.signutaddesktop;

import com.google.gson.*;


/**
 *
 * @author cpereira
 */

/*

{"SPLib":"coisa","Processo":"coisa","Filename":"coisa","WSUrl":"coisa","WSDomain":"intra","WSUser":"coisa","WSPassword":"mudarPass","WSToken":"token"}
{"SPLib":"coisa","Processo":"coisa","Filename":"coisa","WSUrl":"coisa","WSToken":"token"}

*/

public class ArgumentHandler {

    private String SPLib;
    public String getSPLib() {
        return SPLib;
    }
    public void setSPLib(String SPLib) {
        this.SPLib = SPLib;
    }

    private String Processo;
    public String getProcesso() {
        return Processo;
    }
    public void setProcesso(String Processo) {
        this.Processo = Processo;
    }

    private String Filename;
    public String getFilename() {
        return Filename;
    }
    public void setFilename(String Filename) {
        this.Filename = Filename;
    }

    private String WSUrl;
    public String getWSUrl() {
        return WSUrl;
    }
    public void setWSUrl(String WSUrl) {
        this.WSUrl = WSUrl;
    }
/*
    private String WSDomain;
    public String getWSDomain() {
        return WSDomain;
    }
    public void setWSDomain(String WSDomain) {
        this.WSDomain = WSDomain;
    }

    private String WSUser;
    public String getWSUser() {
        return WSUser;
    }
    public void setWSUser(String WSUser) {
        this.WSUser = WSUser;
    }

    private String WSPassword;
    public String getWSPassword() {
        return WSPassword;
    }
    public void setWSPassword(String WSPassword) {
        this.WSPassword = WSPassword;
    }
*/
    private String WSToken;
    public String getWSToken() {
        return WSToken;
    }
    public void setWSToken(String WSToken) {
        this.WSToken = WSToken;
    }
    
    public ArgumentHandler(String rjson)
    {
        Gson g = new Gson();
        
        ArgumentHandler novo = g.fromJson(rjson, ArgumentHandler.class);
        
        SPLib = novo.SPLib;
        Processo = novo.Processo;
        Filename = novo.Filename;
        WSUrl = novo.WSUrl;
        /*
        WSDomain = novo.WSDomain;
        WSUser = novo.WSUser;
        WSPassword = novo.WSPassword;
        */
        WSToken = novo.WSToken;
    }
}
