/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.utad.signutaddesktop;

import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author cpereira
 */
public class TestHandler {
    
    private String message;

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    public TestHandler() {
    }
    
    public TestHandler(String message) {
        this.setMessage(message);
    }
    public void doStuff(){
        JFrame frame = new JFrame("Demo");
        
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        frame.setVisible(true);
        
        JOptionPane.showMessageDialog(frame, "O primeiro argumento do programa é \"" + getMessage() + "\"", "O primeiro argumento é...", JOptionPane.INFORMATION_MESSAGE);
        
        frame.setVisible(false);
        frame.dispose();
    }
}
