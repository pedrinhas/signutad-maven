/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.utad.signutaddesktop;

/**
 *
 * @author cpereira
 */
public class CertificateRevokedException extends Exception {
    public CertificateRevokedException(String msg) { super(msg); }
    
}
