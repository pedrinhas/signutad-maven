/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.utad.signutaddesktop;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.CRLVerifier;
import com.itextpdf.text.pdf.security.CrlClient;
import com.itextpdf.text.pdf.security.CrlClientOnline;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.LtvTimestamp;
import com.itextpdf.text.pdf.security.LtvVerification;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.OCSPVerifier;
import com.itextpdf.text.pdf.security.OcspClient;
import com.itextpdf.text.pdf.security.OcspClientBouncyCastle;
import com.itextpdf.text.pdf.security.PdfPKCS7;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.itextpdf.text.pdf.security.ProviderDigest;
import com.itextpdf.text.pdf.security.TSAClient;
import com.itextpdf.text.pdf.security.TSAClientBouncyCastle;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.sun.security.auth.callback.DialogCallbackHandler;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.AccessController;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStore.Builder;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.security.ProviderException;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.security.auth.callback.CallbackHandler;
import javax.swing.*;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.x509.CRLDistPoint;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.poreid.CardFactory;
import org.poreid.CardNotPresentException;
import org.poreid.CardTerminalNotPresentException;
import org.poreid.POReIDException;
import org.poreid.POReIDSmartCard;
import org.poreid.UnknownCardException;
import org.poreid.config.POReIDConfig;
import org.poreid.crypto.POReIDProvider;
import org.poreid.dialogs.selectcard.CanceledSelectionException;
import sun.security.pkcs11.SunPKCS11;
import sun.security.pkcs11.wrapper.PKCS11Exception;

/**
 *
 * @author cpereira
 */
public class SignUTADDesktop {

    private String url;
    private String lib;
    private String processNo;
    private String filename;
    private String domain;
    private String domainUser;
    private String user;
    private String pass;
    
    private String reasonCaption;
    private String reason;
    private String locationCaption;
    private String location;
    
    private Boolean staging;
    private String authMode;
    
    private String token;
    
    Boolean visibleSignature;
    static String signedProcessSuffix = "_assinado";
    String numberSig;
    private float urx, ury, llx, lly;
    
    
    public static void main(String[] args) 
    {
        String message = "";
        
        try
        {
            String argument = args[0].replace("utadsign://", "");

            byte[] argDecoded = Base64.decode(argument);
            String json = "";

            if (argDecoded == null) argDecoded = Base64.decode(argument.substring(0, argument.length() - 1));

            if (argDecoded != null) json = new String(argDecoded);
            else throw new IllegalArgumentException("A string não está em base64: \"" + args[0] + "\"");

            ArgumentHandler arg = new ArgumentHandler(json);

            //<editor-fold defaultstate="expanded" desc="Assinar">

            byte[] doc = DownloadFile(arg.getWSUrl(), arg.getSPLib(), arg.getProcesso(), arg.getFilename(), arg.getWSToken());

            byte[] signedDoc = SignPDF(doc);
            
            String signedDocName = arg.getFilename().substring(0, arg.getFilename().lastIndexOf('.')) + signedProcessSuffix + arg.getFilename().substring(arg.getFilename().lastIndexOf('.'));
            
            UploadFile(arg.getWSUrl(), arg.getSPLib(), arg.getProcesso(),signedDocName, signedDoc, arg.getWSToken());
            //</editor-fold>
            System.exit(0);
        }
        catch (DocumentException
                | IOException
                | RuntimeException
                | GeneralSecurityException
                | PrivilegedActionException
                | CertificateChainException 
                | CertificateRevokedException 
                | InvalidOSException 
                | PinLockedException ex)
        {
            message = ex.getClass().toString() + " - " + ex.getMessage();
        }
        
        message = message + "";
    }
    
 
    //<editor-fold defaultstate="collapsed" desc="Assinatura">
    
    private static List<String> getCrlDistributionPoints(X509Certificate cert) throws CertificateParsingException, IOException {        
        byte[] crldpExt = cert
                .getExtensionValue(X509Extensions.CRLDistributionPoints.getId());
        if (crldpExt == null) {
            return new ArrayList<String>();
        }
        ASN1InputStream oAsnInStream = new ASN1InputStream(
                new ByteArrayInputStream(crldpExt));
        ASN1Primitive derObjCrlDP = oAsnInStream.readObject();
        DEROctetString dosCrlDP = (DEROctetString) derObjCrlDP;
        byte[] crldpExtOctets = dosCrlDP.getOctets();
        ASN1InputStream oAsnInStream2 = new ASN1InputStream(
                new ByteArrayInputStream(crldpExtOctets));
        ASN1Primitive derObj2 = oAsnInStream2.readObject();
        CRLDistPoint distPoint = CRLDistPoint.getInstance(derObj2);
        List<String> crlUrls = new ArrayList<String>();
        for (DistributionPoint dp : distPoint.getDistributionPoints()) {
            DistributionPointName dpn = dp.getDistributionPoint();
            // Look for URIs in fullName
            if (dpn != null
                && dpn.getType() == DistributionPointName.FULL_NAME) {
                GeneralName[] genNames = GeneralNames.getInstance(
                        dpn.getName()).getNames();
                // Look for an URI
                for (int j = 0; j < genNames.length; j++) {
                    if (genNames[j].getTagNo() == GeneralName.uniformResourceIdentifier) {
                        String url = DERIA5String.getInstance(
                                genNames[j].getName()).getString();
                        crlUrls.add(url);
                    }
                }
            }
        }
        return crlUrls;
    }
   
    private static final class PrivateKeyAndChain{
        private PrivateKey pk;
        private Certificate[] chain;

        public PrivateKeyAndChain(PrivateKey pk, Certificate[] chain) {
            this.pk = pk;
            this.chain = chain;
        }
        
        public PrivateKey getPk() {
            return pk;
        }

        public Certificate[] getChain() {
            return chain;
        }
    } 
    
    private static byte[] SignPDF(byte[] srcBytes)
            throws IOException
            , DocumentException
            , GeneralSecurityException
            , InvalidOSException
            , CertificateExpiredException
            , CertificateNotYetValidException
            , CertificateRevokedException
            , KeyStoreException
            , NoSuchAlgorithmException
            , UnrecoverableKeyException
            , PrivilegedActionException, PinLockedException, CertificateChainException 
    {
        ByteArrayInputStream src = new ByteArrayInputStream(srcBytes);
        
        PrivateKey pk = null;
        Certificate[] chain = null;
                
        //<editor-fold defaultstate="collapsed" desc="Com privlegios">

        
        /*final PrivateKeyAndChain tempPKAC = AccessController.doPrivileged(new PrivilegedExceptionAction<PrivateKeyAndChain>() {

            @Override
            public PrivateKeyAndChain run() throws Exception {
                Security.addProvider(new POReIDProvider());
                KeyStore ks = KeyStore.getInstance(POReIDConfig.POREID);
                ks.load(null,null);

                PrivateKey _pk = (PrivateKey)ks.getKey(POReIDConfig.ASSINATURA, null);
                Certificate[] _chain = ks.getCertificateChain(POReIDConfig.ASSINATURA);
                
                PrivateKeyAndChain pkac = new PrivateKeyAndChain(_pk, _chain);
                
                return pkac;
            }
        });
        
        pk = tempPKAC.getPk();
        chain = tempPKAC.getChain();*/
        
        KeyStore ks = null;
        
        String osName = System.getProperty("os.name");
        String osArch = System.getProperty("os.arch");
        String pkcs11configCC = "";

        String assinaturaCertifLabel = "";
        String provider = "";
        
        if (-1 != osName.indexOf("Mac"))
        {
            pkcs11configCC = "name=SmartCard\nattributes=compatibility\nlibrary=/usr/local/lib/pteidpkcs11.1.61.0.dylib\n";
            assinaturaCertifLabel = "CITIZEN SIGNATURE CERTIFICATE";
            byte[] pkcs11configBytes = pkcs11configCC.getBytes();
            ByteArrayInputStream configStream = new ByteArrayInputStream(pkcs11configBytes);

            SunPKCS11 pkcs11Provider = new SunPKCS11(configStream);
            Security.addProvider(pkcs11Provider);

            CallbackHandler cmdLineHdlr = new DialogCallbackHandler();

            Builder builder = KeyStore.Builder.newInstance("PKCS11", pkcs11Provider, new KeyStore.CallbackHandlerProtection(cmdLineHdlr));
            ks = builder.getKeyStore();
            
            assinaturaCertifLabel = "CITIZEN SIGNATURE CERTIFICATE";
            //provider = "SunPKCS11";
            provider = ks.getProvider().getName();

        }
        else
        {
            if(Security.getProvider(POReIDConfig.POREID) == null) Security.addProvider(new POReIDProvider());
            ks = KeyStore.getInstance(POReIDConfig.POREID);

            assinaturaCertifLabel = POReIDConfig.ASSINATURA;
            provider = POReIDConfig.POREID;
        }        
        
        ks.load(null, null);

        pk = (PrivateKey)ks.getKey(assinaturaCertifLabel, null);
        chain = ks.getCertificateChain(assinaturaCertifLabel);
        
        if(chain == null)
        {        
            Security.removeProvider(POReIDConfig.POREID);
            Security.addProvider(new POReIDProvider());

            ks = KeyStore.getInstance(POReIDConfig.POREID);
            ks.load(null,null);

            pk = (PrivateKey)ks.getKey(POReIDConfig.ASSINATURA, null);
            chain = ks.getCertificateChain(POReIDConfig.ASSINATURA);
            
            if(chain == null)
            {
                throw new CertificateChainException();
            }
        }

        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Sem privlegios">
        /*
        Security.addProvider(new POReIDProvider());
        KeyStore ks = KeyStore.getInstance(POReIDConfig.POREID);
        ks.load(null,null);

        pk = (PrivateKey)ks.getKey(POReIDConfig.ASSINATURA, null);
        chain = ks.getCertificateChain(POReIDConfig.ASSINATURA);
        */
        
        //</editor-fold>

        // reader and stamper
        PdfReader reader = new PdfReader(src);
        
        ByteArrayOutputStream dest = new ByteArrayOutputStream();
        PdfStamper stamper = PdfStamper.createSignature(reader, dest, '\0', null, true);
        //PdfAStamper stamper = PdfAStamper.createSignature(reader, dest, '\0', PdfAConformanceLevel.PDF_A_3B);

        // appearance
        PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
        /*
        appearance.setReasonCaption(reasonCaption);
        appearance.setReason(reason);
        appearance.setLocationCaption(locationCaption);
        appearance.setLocation(location);
        */
        
        
        //Image utadLogo = Image.getInstance("C:\\Users\\cpereira\\Desktop\\Git repositories\\AppletAssinatura\\img\\utad_ass3.png");
        
        //ppearance.setImage(utadLogo);
        //appearance.setImage(utadLogo);
        //appearance.setImageScale(0.5f);

        /*
        if(visibleSignature) 
        {
            appearance.setVisibleSignature(new Rectangle(llx, lly, urx, ury), 1, "sig" + numberSig);
        }
        */
        appearance.setSignatureCreator("Serviços de Informática e Comunicações - UTAD");
        //appearance.setCertificate(chain[0]);
        
        // digital signature
        ExternalSignature es = new PrivateKeySignature(pk, "SHA-512", provider);
        ExternalDigest digest = new ProviderDigest(null);
        
        //Timestamp
        TSAClient tsa = new TSAClientBouncyCastle("http://ts.cartaodecidadao.pt/tsa/server", "", "");
        /*//OCSP
        OcspClient ocsp = new OcspClientBouncyCastle();
        //CRL
        List<CrlClient> crlList = new ArrayList<CrlClient>();
        crlList.add(new CrlClientOnline(chain));*/
        
        try
        {
            MakeSignature.signDetached(appearance, digest, es, chain, null, null, tsa, src.available(), MakeSignature.CryptoStandard.CMS);
            
            src.close();
            
            stamper.close();
            reader.close();

            byte[] res = MakeLTV(dest.toByteArray());

            return res;
        }
        catch (IOException | DocumentException | GeneralSecurityException ex)
        {
            throw ex;
        }
        catch (ProviderException ex)
        {  
            if(ex.getCause().getClass() == PKCS11Exception.class)
            {
                if(ex.getMessage().equals("sun.security.pkcs11.wrapper.PKCS11Exception: CKR_PIN_LOCKED")) throw new PinLockedException();
                else throw ex;
            }
        }
        catch(Throwable t)
        {    
            throw t;
        }
        
        return null;
    }
    
    
    
    private static byte[] MakeLTV(byte[] src) throws IOException, GeneralSecurityException, DocumentException, PrivilegedActionException
    {
        OcspClientBouncyCastle ocsp = new OcspClientBouncyCastle();
        CrlClientOnline crl = new CrlClientOnline();
        TSAClientBouncyCastle tsa = new TSAClientBouncyCastle("http://ts.cartaodecidadao.pt/tsa/server");
        
        //ORIGEM

        PdfReader readerLtv = new PdfReader(src);

        //DESTINO
        
        ByteArrayOutputStream dest = new ByteArrayOutputStream();

        //PdfStamper stp = PdfStamper.createSignature(readerLtv, dest, '\0', null, true);

        PdfStamper stp = new PdfStamper(readerLtv, dest, '\0', true);

        
        AccessController.doPrivileged(new PrivilegedExceptionAction() {
            @Override
            public Object run() throws Exception {
                return Security.addProvider(new BouncyCastleProvider());
            }
        });

        LtvVerification ltvVer = stp.getLtvVerification();

        AcroFields fields = stp.getAcroFields();

        List<String> names = fields.getSignatureNames();
        String sigName = names.get(names.size() - 1);

        ltvVer.addVerification(sigName, ocsp, crl,
                        LtvVerification.CertificateOption.WHOLE_CHAIN,
                        LtvVerification.Level.OCSP_OPTIONAL_CRL,
                        LtvVerification.CertificateInclusion.NO);   

        //PdfSignatureAppearance sapLtv = stp.getSignatureAppearance();
        //LtvTimestamp.timestamp(sapLtv, tsa, null);

        //readerLtv.close();
        stp.close();
        
        //byte[] finalBytes = MakeLTVOnTimestamp(dest.toByteArray());
        
        return dest.toByteArray();
    }
    
    private static byte[] MakeLTVOnTimestamp(byte[] src) throws IOException, DocumentException, PrivilegedActionException, GeneralSecurityException
    {
        OcspClientBouncyCastle ocsp = new OcspClientBouncyCastle();
        CrlClientOnline crl = new CrlClientOnline();
        
        //ORIGEM

        PdfReader readerLtv = new PdfReader(src);

        //DESTINO
        
        ByteArrayOutputStream dest = new ByteArrayOutputStream();

        PdfStamper stp = new PdfStamper(readerLtv, dest, '\0', true);

        AccessController.doPrivileged(new PrivilegedExceptionAction() {
            @Override
            public Object run() throws Exception {
                return Security.addProvider(new BouncyCastleProvider());
            }
        });

        LtvVerification ltvVer = stp.getLtvVerification();

        AcroFields fields = stp.getAcroFields();

        List<String> names = fields.getSignatureNames();
        String sigName = names.get(names.size() - 1);

        ltvVer.addVerification(sigName, ocsp, crl,
                LtvVerification.CertificateOption.SIGNING_CERTIFICATE,
                LtvVerification.Level.OCSP_OPTIONAL_CRL,
                LtvVerification.CertificateInclusion.NO);
        
        stp.close();
        readerLtv.close();
        
        
        return dest.toByteArray();
    }
    
//</editor-fold>
   
    
    //<editor-fold defaultstate="collapsed" desc="Upload/Download">    
    //<editor-fold defaultstate="collapsed" desc="REST">
    
    private static String Trim(String text, String toReplace) //DONE
    {
        while(text.endsWith(toReplace))
        {
            text = text.replaceAll(toReplace + "$|^" + toReplace, "");
        }
        
        return text;
    }
    
//</editor-fold>
    
    private static byte[] DownloadFile(String url, String lib, String processNo, String filename, String token) throws MalformedURLException, IOException, RuntimeException, PrivilegedActionException //DONE
    {      
        url = Trim(url, "/");
        filename = filename.replace(" ", "%20");

        //URL requestAddress = new URL((url + "/_api/Web/GetFolderByServerRelativeUrl%28%27" + listName + "/" + processNo + "%27%29/Files%28%27" + filename + "%27%29/$value").replace(" ", "%20"));

        URL requestAddress = new URL(url + "/getFile(" + lib + "/" + processNo + "/" + filename + ")?token=" + token);



        HttpURLConnection conn = (HttpURLConnection) requestAddress.openConnection();

        conn.setRequestMethod("GET");
        //Só é preciso no upload
        //String digest = GetSPDigest(url, domainUser, pass);
        //conn.setRequestProperty("X-RequestDigest", digest);

        conn.setRequestProperty("Accept", "application/pdf");

        //if(!SetAuthentication(url, conn)) return null;

        conn.connect();

        int respcode = conn.getResponseCode();

        if (respcode > 399) 
        {
            throw new RuntimeException("Failed : HTTP error code : " + respcode);
        }

        InputStream in = conn.getInputStream(); 
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        byte[] b = new byte[1024];
        int count;

        while ((count = in.read(b)) > 0)
        {
            out.write(b, 0, count);
        }

        out.close();

        return out.toByteArray();


        //return AccessController.doPrivileged(new DownloadPrivilegedAction(url, processNo, filename, token));
    }
    
    private static void UploadFile(String url, String lib, String processNo, String filename, byte[] file, String token) throws MalformedURLException, IOException, PrivilegedActionException //DONE
    {
        url = Trim(url, "/");
        filename = filename.replace(" ", "%20");

        URL requestAddress = new URL((url + "/AddFileAuth(" + lib + "/" + processNo + "/" + filename + ")?categoria=teste&assinado=true&token=" + token).replace(" ", "%20"));

        HttpURLConnection conn = (HttpURLConnection) requestAddress.openConnection();

        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Length", file.length + "");
        conn.setRequestProperty("Content-Type", "application/pdf");
        /*
        String digest = GetSPDigest(url, domainUser, pass);

        if(digest.equals("")) throw new ConnectException("Can't get form digest from Sharepoint");

        conn.setRequestProperty("X-RequestDigest", digest);
        */

        //if(!SetAuthentication(url, conn)) return;

        conn.setRequestProperty("Accept", "application/json;odata=verbose");

        conn.setDoOutput(true);

        OutputStream os = conn.getOutputStream();

        os.write(file);

        int respcode = conn.getResponseCode();

        if (respcode > 399) 
        {
            throw new RuntimeException("Failed : HTTP error code : " + respcode);
        }
        
        //AccessController.doPrivileged(new UploadPrivilegedAction(url, processNo, filename, file, token));
    }
    //</editor-fold>
}
